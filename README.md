# Anarchist groups, anarcho syndicalist unions & revolutionary unions

Awesome list of active anarchist (and alike) organizations.  
Submit your additions/suggestions/whatever via a new PR/MR or reach out at [aga@agamsterdam.org](mailto:aga@agamsterdam.org).

## Index

+ [Argentina](#argentina)
+ [Australia](#australia)
+ [Austria](#austria)
+ [Bangladesh](#bangladesh)
+ [Belarus](#belarus)
+ [Belgium](#belgium)
+ [Brazil](#brazil)
+ [Bulgaria](#bulgaria)
+ [Canada](#canada)
+ [Croatia](#croatia)
+ [Czech republic](#czech-republic)
+ [Cyprus](#cyprus)
+ [Denmark](#denmark)
+ [Ecuador](#ecuador)
+ [Estonia](#estonia)
+ [Finland](#finland)
+ [France](#france)
+ [Germany](#germany)
+ [Greece](#greece)
+ [Iceland](#iceland)
+ [India](#india)
+ [Indonesia](#indonesia)
+ [Kurdistan](#kurdistan)
+ [Latvia](#latvia)
+ [Iran](#iran)
+ [Ireland](#ireland)
+ [Italy](#italy)
+ [Mexico](#mexico)
+ [Netherlands](#netherlands)
+ [Norway](#norway)
+ [Philippines](#philippines)
+ [Poland](#poland)
+ [Portugal](#portugal)
+ [Romania](#romania)
+ [Russia](#russia)
+ [Serbia](#serbia)
+ [Slovakia](#slovakia)
+ [South Africa](#south-africa)
+ [Spain](#spain)
+ [Sweden](#sweden)
+ [Turkey](#turkey)
+ [Ukrania](#ukrania)
+ [Uruguay](#uruguay)
+ [United Kingdom](#united-Kingdom)
+ [United States of America](#united-states-of-america)
+ [World wide](#world-wide)
+ [Others](#others)

---

## Countries

---
### Argentina

#### Federacion libertaria argentina

http://federacionlibertariaargentina.org/home.html


#### Red libertaria de Buenos Aires

http://www.redlibertaria.com.ar/


---
### Australia

#### Anarcho-Syndicalist Federation

http://asf-iwa.org.au/


#### IWW australia

http://iww.org.au/


#### Collective Action

http://www.collectiveaction.org.au


#### Melbourne Anarchist Communist Group

https://melbacg.wordpress.com/about/


#### Melbourne Antifascists

https://melbourneantifascists.wordpress.com


#### Red & Black Notes

https://www.redblacknotes.com/


#### ABC Melbourne

https://abcmelb.wordpress.com/


---
### Austria

#### Wiener ArbeiterInnen Syndikat

https://wiensyndikat.wordpress.com/

#### IWW Austria 

https://iww.or.at/


#### ABC Austria 

https://www.abc-wien.net/


---
### Bangladesh

#### Bangladesh Anarcho-Syndicalist Federation

https://www.bangladeshasf.org/


---
### Belarus

#### Belarus Anarchist Black Cross

https://abc-belarus.org


---
### Belgium

#### Leuven Anarchistische Groep

https://leuvenag.noblogs.org/


#### Anarchistisch Kollektief Gent

https://anarchistischkollektief.wordpress.com/


#### Anarchistisch Collectief Antwerpen

https://facebook.com/anarchistischcollectiefantwerpen


---
### Brazil

#### Anarkio

https://anarkio.net/index.php/contato/


#### Iniciativa Federalista Anarquista no Brasil

https://ligarj.wordpress.com/category/iniciativa-federalista-anarquista-brasil/


#### Federação das Organizações Sindicalistas Revolucionárias do Brasil

https://lutafob.wordpress.com/


#### Federação Anarquista Gaúcha / Fórum do Anarquismo Organizado (FAO)

http://www.vermelhoenegro.co.cc/


#### Federação Anarquista do Rio de Janeiro / FAO 

http://www.farj.org/


#### C.O.B

http://cob-ait.net/


#### Cruz Negra Anarquista Rio de Janeiro

https://cnario.noblogs.org/


---
### Bulgaria

#### Autonomous Workers' Confederation (ARK) 

http://avtonomna.com/


#### Federation of Anarchists in Bulgaria

https://www.anarchy.bg/

  
#### Confederation of Independent Trade Unions of Bulgaria  (KNSB/CITUB)

http://www.knsb-bg.org/


#### ARS (AIT)

http://arsindikat.org/


---
### Canada

#### IWW Canada

https://industrialworker.org/


#### Common cause

http://linchpin.ca/


#### BCBlackOut - Social War Against Industrial Expansion 

https://bcblackout.wordpress.com/


#### Montreal Counter-Information

https://mtlcounterinfo.org


#### North Shore Counter-Info

https://north-shore.info


#### Solidarity Across Borders

https://www.solidarityacrossborders.org/


#### Victoria Anarchist Reading Circle

https://victoriaanarchistreadingcircle.ca


#### Warrior Publications

https://warriorpublications.wordpress.com


#### 4 Struggle Mag

https://4strugglemag.org/


---
### Croatia

#### MASA (AIT)

https://masa-hr.org/

---
### Czech republic

#### Socialista solidarita

https://organizace.socsol.cz/


#### Anarchistická federace 

https://www.afed.cz/


#### Kollektiv 115

https://k115.org/


#### ABC Czech

https://abcnews.noblogs.org/


---
### Cyprus

#### Syspirosi Atakton

https://syspirosiatakton.org/


---
### Denmark

#### Libertære Socialister

http://libsoc.dk/


---
### Ecuador

#### Chisque anarquista

http://chasquianarquista.blogspot.com/


---
### Estonia


---
### Finland

#### Takku

https://takku.net


---
### France

#### Union Antifasciste Toulousaine

https://unionantifascistetoulousaine.wordpress.com/


#### Collectif Antifasciste de Besançon 

https://cabesancon.wordpress.com/


#### Collectif Vigilance 69

http://collectifvigilance69.over-blog.com/


#### Réseau Angevin Antifasciste 

https://raaf.noblogs.org/


#### Fédération Anarchiste 

https://federation-anarchiste.org/


#### Union communiste libertaire 

https://unioncommunistelibertaire.org/


#### Section carrément anti-Le Pen 

http://nopasaran.samizdat.net/


#### CNT 

http://www.cnt-f.org/


#### Attaque

https://attaque.noblogs.org


#### Anarchist Bure Cross

https://anarchistburecross.noblogs.org/


---
### Germany

#### Gefangenensolidarität Jena

https://gefangenensolijena.noblogs.org/


#### ABC Jena

https://abcj.blackblogs.org/


#### ABC Berlin

https://www.abc-berlin.net/


#### ABC Rhineland

https://abcrhineland.blackblogs.org/


#### ABC Dresden

https://abcdd.org/


#### IWW Germany

https://www.wobblies.org


#### FAU 

https://www.fau.org/


#### Föderation deutschsprachiger AnarchistInnen/ Federation of German speaking Anarchists

https://fda-ifa.org/


#### Gefangenen Gewerkschaft-Bundesweite Organisation

https://ggbo.de/


#### …ums Ganze!

https://www.umsganze.org/


#### Efya

https://eyfa.org/


---
### Greece

#### Rocinante

https://rocinante.gr/


#### Libertarian Syndicalist Union - ESE (CIT)

https://ese.espiv.net/ 


#### Workers Grassroot Federation

https://ergova.wordpress.com/ 


#### Union of waitors/waitresses/cooks and workers in restaurants (Athens)

http://somateioserbitoronmageiron.blogspot.com/ 


#### Union of waitors/waitresses/cooks and workers in restaurants (Thessaloniki)

https://ssmthess.espivblogs.net/ 


#### Grassroot Assembly of Motorbike Workers

http://sveod.gr/ 


#### Union of Employees in Book – Paper – Digital Media industry (Attiki)

https://bookworker.wordpress.com/ 


#### Grassroot Union of Employees in NGOs

https://svemko.espivblogs.net/ 


#### Grassroot Union of Employees in the mental health and social providence field

https://svepsykoi.espivblogs.net/ 


#### Union of Translators – Correctors

http://www.smed.gr/ 


#### National Union of Employees in Vodafone

http://www.pasevodafone.gr/ 


#### National Union of Employees in Wind

http://pasetim.com/ 


#### Assembly of employees/unemployed/students in media

http://katalipsiesiea.blogspot.com/ 


#### Assembly of employees/unemployed in dial/call lines

https://proledialers.espivblogs.net/ 


#### “Orthostasia” employees in shops

https://orthostasia.wordpress.com/ 


#### “Class Front” Initiative of employees in public transport

https://taxikometopo.wordpress.com/ 


#### Αναρχική Πολιτική Οργάνωση / Anarchist Political Organisation

http://apo.squathost.com/


#### Αντιεξουσιαστική Κίνηση / Anti-Authoritarian Movement

https://www.antiauthoritarian.gr/


#### Prosfygıka community

https://sykaprosquat.noblogs.org


#### You can't evict a movement

https://cantevictsolidarityenglish.noblogs.org/


---
### Iceland

#### IWW Iceland

https://iwwisland.org/


---
### India

#### Muktivadi Ekta Morcha

https://muktivadi.blackblogs.org/


---
### Indonesia

#### Persaudaraan Pekerja Anarko Sindikalis

https://ppasonline.wordpress.com/


#### Rimpang

https://rimpang.noblogs.org/


#### Palang Hitam

https://palanghitamanarkis.noblogs.org/


#### Federasi Anti Otoritarian

fed.ao@protonmail.com


---
### Iran

#### Anarchist Union of Afghanistan & Iran

http://asranarshism.com/


---
### Ireland

#### Workers Solidarity Movement 

http://www.wsm.ie/


#### Solidarity federation 

http://www.solfed.org.uk/local/belfast


#### IWW Ireland

https://onebigunion.ie/


#### Anarchist Black Cross Ireland

https://abcireland.wordpress.com


#### Derry Anarchist Collective

https://derryanarchists.blogspot.com/


---
### Italy

#### Alternativa Libertaria

http://alternativalibertaria.fdca.it/


#### Croce Nera Anarchica

https://www.autistici.org/cna/


#### Federation of Anarchist Communists 

http://www.fdca.it/


#### CIB UNICOBAS (Confederazione Italiana di Base) 

https://www.unicobas.org/


#### Federazione Anarchica Italiana 

http://www.federazioneanarchica.org/


#### umanita nova 

https://umanitanova.org/


#### Union of Anarchist Communist of Italy 

http://www.ucadi.org/


#### Unione Sindacale Italiana 

https://www.unionesindacaleitaliana.eu/


#### USI

https://usi-cit.org/


#### IWW Italy

https://iwwita.it/


---
### Kurdistan

####  Tekoşîna Anarşîst

https://tekosinaanarsist.noblogs.org


---
### Latvia


---
### Mexico

#### Federation of Anarchists in Mexico / Federación Anarquista de México

http://federacionanarquistademexico.org/


#### Voces Oaxaqueñas Construyendo Autonomía y Libertad (Mexico)

http://vocal.saltoscuanticos.org/


---
### Netherlands

#### Anarchistische Groep Amsterdam

http://www.agamsterdam.org/


#### Anarchistische Groep Nijmegen

https://www.anarchistischegroepnijmegen.nl/


#### Anarchist Black Cross (ABC) Nijmegen

https://abcnijmegen.wordpress.com/


#### Autonomen Den Haag

https://autonomendenhaag.wordpress.com/


#### Brabantse Anarchistische Kring

https://brabantseak.noblogs.org/


#### Doorbraak

http://www.doorbraak.eu/


#### Groenfront

https://www.groenfront.nl/


#### Autonomen Brabant

http://autonomenbrabant.nl/


#### Vloerwerk

https://vloerwerk.org/


#### Fairwork

https://www.fairwork.nu/en/homepage/


#### Vrije Bond

http://www.vrijebond.org


#### Opstand

https://opstand.noblogs.org


#### The Barricade

https://thebarricade.noblogs.org


---
### Norway

#### Motmakt

http://motmakt.no/


#### NSF

http://www.nsf-iaa.org/


---
### Philippines

#### Bandilang Itim

https://bandilangitim.noblogs.org


---
### Poland

#### Anarchistyczny Czarny Krzyż / ABC Poland

https://ack.org.pl/


#### ZSP (AIT)

http://zsp.net.pl


#### Inicjatywa Pracownicza / Workers Initiative (CIT) 

https://www.ozzip.pl/


#### Spina

https://spina.noblogs.org/


---
### Portugal

#### AIT-Secção Portuguesa

http://ait-sp.blogspot.com/


#### Guilhotina

https://guilhotina.info/en/


---
### Romania

#### Ravna

https://iasromania.wordpress.com/


#### Baldovin concept

http://baldovinconcept.blogspot.com/


#### Dreptul la Oras(DO) 

http://dreptullaorastimisoara.com/


#### E-Romnja (Rroma minority related)

www.e-romnja.ro


---
### Russia

#### ABC Irkutsk

https://abc38.noblogs.org/


#### Confederation of Revolutionary Anarcho-Syndicalists 

https://aitrus.info/


#### Anarchist Black Cross Russia

https://avtonom.org


#### Revolutionary Action

https://revbel.org/en/


---
### Serbia 

#### ISA (AIT)


#### ASI-MUR

https://inicijativa.org/tiki/tiki-index.php?page=ASI+English


---
### Slovakia

#### Priama Akcia (AIT)

https://www.priamaakcia.sk/


---
### South Africa

#### Zabalaza

https://zabalaza.net/


---
### Spain

#### Cruz Negra Anarquista

https://cruznegraanarquista.noblogs.org/


#### CNT 

https://www.cnt-ait.org/


#### CGT

http://cgt.org.es/


#### Solidaridad Obrera

https://solidaridadobrera.org/


#### Mierda jobs

https://twitter.com/JobsMierda


---
### Sweden

#### ABC Gothenburg

https://abcgbg.net/


#### Antifa Sweden

https://antifa.se/


#### SAC 

https://www.sac.se/en/


#### Swedish Anarcho-syndicalist Youth Federation 

www.suf.cc


#### OLS 

https://orestadls.wordpress.com/


#### Gatorna

https://gatorna.info/about/


---
### Turkey

#### IWW Turkey

https://orestadls.wordpress.com/


#### Anarsist Federasyon.

https://anarsistfederasyon.org/


#### Devrimci Anarşist Faaliyet

https://anarsistfaaliyet.org/


#### Komun academy

https://komun-academy.com


---
### Switzerland

#### IWW Switzerland

https://www.wobblies.org/


#### Libertäre Aktion Winterthur

http://libertaere-aktion.ch/


#### Organisation Socialiste Libertaire

http://www.rebellion.ch/


---
### Uruguay

#### Colectivo Socialista Libertaria

http://periodicorojoynegro.blogspot.com/


---
### Ukrania

#### Autonoms Workers Union 

Big change dont exist anymore, because of civil war.


#### Borotba

http://www.borotba.su/


---
### United Kingdom

#### IWW UK, Ireland

https://iww.org.uk/


#### London Antifascists

https://londonantifascists.wordpress.com/


#### North London Antifascists

https://northlondonantifa.wordpress.com/


#### Anarchist Federation UK

http://afed.org.uk/


#### Class War 

https://classwar.uk/


#### Freedompress 

https://freedompress.org.uk/


#### Solidarity federation 

http://www.solfed.org.uk/


#### Alternative Bristol

https://alternativebristol.com/


#### Anarchist Action Network

https://network23.org


#### Workers wild west

https://workerswildwest.wordpress.com/


#### Angry workers of the world

https://www.angryworkers.org


#### Antifascist Network

https://antifascistnetwork.org


#### Brighton Antifascists

https://brightonantifascists.com


#### Leeds Antifascists Network

https://leedsantifascists.wordpress.com/


#### 3 Cafa

https://3cafa.wordpress.com/


#### ABC Brighton

https://www.brightonabc.org.uk/


#### Bristol Anarchist Black Cross

https://bristolabc.wordpress.com/


#### Green and Black Cross

https://greenandblackcross.org/


#### Brighton Solidarity Federation

http://www.brightonsolfed.org.uk


#### Cautiously pessimistic

https://nothingiseverlost.wordpress.com


#### Anarchist Federation Glasgow

https://glasgowanarchists.wordpress.com/


#### Green Anticapitalist Media

https://greenanticapitalist.org


#### Haringey solidarity group

http://www.haringey.org.uk/content/


#### IWW Bristol

https://iww.org.uk/bristol/


#### IWW Scotland

https://iwwscotland.wordpress.com/


#### Liverpool Solidarity Federation

https://liverpoolsf.org/


#### London Anarchist Communist Group

https://londonacg.blogspot.com/


#### London Anarchist Federation

https://aflondon.wordpress.com/


#### No borders Nottingham

https://www.nobordersnottingham.org.uk/


#### No Fixed Abode Anti-Fascists (NFAAF)

https://nfaaf.wordpress.com


#### North East Anarchist Group

https://northeastanarchistgroup.org


#### Red & Black Leeds

https://wearetherabl.wordpress.com/


#### Southampton Solfed

https://southamptonsolfed.wordpress.com/


#### Surrey Anarchist Communist Group

https://surreyanarchistcommunistgroup.blogspot.com/


#### Wessex solidarity

https://wessexsolidarity.wordpress.com


#### The commoner

https://www.thecommoner.org.uk


#### The sparrows' nest

https://www.thesparrowsnest.org.uk


#### Anarchist Communist Group

https://www.anarchistcommunism.org/


#### Dywizjon 161

https://dywizjon161.wordpress.com/about-2/


---
### United States of America

#### Black Rose/Rosa Negra

https://blackrosefed.org/locals/


#### Sacramento Prisioner Support

https://sacprisonersupport.wordpress.com/


#### Antifa Sacramento

https://antifasac.blackblogs.org/


#### Youth liberation

https://youthliberation.noblogs.org/


#### Youth Liberation Front

https://pnwylf.noblogs.org/


#### Workers Solidarity Alliance

https://workersolidarity.org/


#### A World Without Police

http://aworldwithoutpolice.org/


#### Black Autonomy Action

https://blackautonomynetwork.noblogs.org/


#### The Center for a Stateless Society

https://c4ss.org/about


#### Central Oregon Antifascism

https://centraloregonantifascist.noblogs.org/


#### Central PA Antifa

https://centralpaantifascist.wordpress.com


#### Food Not Bombs Chehalis River

https://crfnb.noblogs.org/post/2020/12/08/our-first-successful-meal/


#### Chehalis River Mutual Aid

https://chehalisrivermutualaid.noblogs.org


#### CVAntifa

https://cvantifa.noblogs.org


#### FireStorm on fascists

https://firestormonfash.noblogs.org/


#### Ft. Lauderdale Youth Liberation Front

https://flaylf.noblogs.org/a-guide-to-documenting-protests/


#### CrimethInc

https://crimethinc.com/about


#### Libertarian Socialist Caucus

https://dsa-lsc.org/


#### Haleyville Anti-Rapist/Anti-Fascist Action

https://haleyvilleara.blackblogs.org


#### IdaVox

http://idavox.com


#### It's going down

https://itsgoingdown.org


#### Long Beach Antifascists

https://lbcafa.noblogs.org


#### Northern California Anti-Racist Action

https://nocara.blackblogs.org/


#### NYC Anarchist Black Cross

https://nycabc.wordpress.com/


#### NYC Antifa

https://nycantifa.wordpress.com/


#### Oakland Abolition And Solidarity

https://oaklandabosol.org


#### Pacific Northwest Community Action Network

https://pnwcommunityactionnetwork.noblogs.org/


#### Paper Revolution

https://www.paperrevolution.org/


#### Philly Anti-Capitalist

https://phlanticap.noblogs.org/


#### Philly Antifa

https://phillyantifa.org/


#### Portland General Defense Committee

https://pdxgdc.com/


#### Redneck Revolt

https://www.redneckrevolt.org


#### Rose City Counter-Info

https://rosecitycounterinfo.noblogs.org/


#### Rural Organizing And Resilience

https://ruralorganizing.wordpress.com/


#### Screwston Anti-Fascist Committee

https://screwstonafc.noblogs.org/


#### Utah Antifascists

https://utah161.noblogs.org


#### West Michigan Anarchist Federation

https://wmaf.noblogs.org/


#### The final straw radio

https://thefinalstrawradio.noblogs.org


#### IWW Washington D.C.

http://www.dciww.org/


#### IWW Detroit

https://www.iwwdetroitgmb.net/


#### IWW New Jersey

https://newjerseyiww.org


#### IWW Portland

https://portlandiww.org/


#### Torch Antifa Network

https://torchantifa.org


#### Torchlight Pittsburgh

https://torchlight.noblogs.org


#### Anti-Racist Action Los Angeles

https://antiracist.org/about/


#### Unity and Struggle

http://www.unityandstruggle.org


#### Tucson Anarchist Black Cross

https://tucsonabc.wordpress.com/


#### Bloomington Anarchist Black Cross

https://bloomingtonabc.noblogs.org/


#### Boston Anarchist Black Cross

https://bostonanarchistblackcross.wordpress.com/


#### Atlanta Anarchist Black Cross

https://www.atlblackcross.org/


#### Blue Ridge

https://brabc.blackblogs.org/


#### ABC Philadelphia

https://phillyabc.org


#### Denver ABC

https://denverabc.wordpress.com/


---
### World wide

#### ICL- CIT

https://www.icl-cit.org/
https://www.iclcit.org/


#### AIT

https://iwa-ait.org/


#### IFA

https://i-f-a.org/


#### IWW

https://iww.org/directory/


#### Anarchist Black Cross Federation

https://www.abcf.net


#### All AIT IWA groups and frends group

https://iwa-ait.org/content/addresses


#### Solidarity International

https://solidarity.international


#### Global May Day

https://globalmayday.net


#### Indigenous action

https://www.indigenousaction.org


#### Indigenous Anarchist Federation

https://iaf-fai.org


#### Indigenous mutual aid

https://www.indigenousmutualaid.org/


#### International Anti-Fascist Defence Fund

https://intlantifadefence.wordpress.com/


#### International Labour Network of Solidarity and Struggles

http://www.laboursolidarity.org


#### Libcom

https://libcom.org


#### Organic Radicals

https://orgrad.wordpress.com/


#### Radical Guide

https://www.radical-guide.com/


#### Riseup 4 Rojava

https://riseup4rojava.org/


#### Stop the war on migrants

https://stopthewaronmigrants.noblogs.org/


#### Sub.media

https://sub.media


#### Subversion 1312

https://www.subversion1312.org


#### Syndicalism.org

https://syndicalism.org/


#### System change not climate change

https://systemchangenotclimatechange.org


#### Warrior Up

https://warriorup.noblogs.org


#### Warzone Distro

https://warzonedistro.noblogs.org


#### We AntiFascists

https://weantifascists.com/


#### The Anarchist Library

https://theanarchistlibrary.org


#### Unicorn Riot

https://unicornriot.ninja


#### Unoffensive Animal

https://unoffensiveanimal.is


#### Ideas and Action

https://ideasandaction.info/


#### Crimethinc

https://crimethinc.com/


#### Antifa International

https://antifainternational.tumblr.com


---
### Others

The equivalent to AIT but non-anarchist based: https://www.etuc.org/en/national-trade-union-confederations-list-member-organisations  


---
### Bibliography

+ https://en.wikipedia.org/wiki/Anarcho-syndicalism  
+ https://zabalaza.net/organise/the-anarkismo-network/  
+ http://anarkismo.net/about_us  
+ https://www.anarchisme.nl/organisaties  
+ https://www.reddit.com/r/Anarchism/comments/kshso6/questions_about_activism/gigpsyz/?utm_source=reddit&utm_medium=web2x&context=3  
+ https://iwa-ait.org/content/addresses  
+ https://www.anarchistfederation.net/sources-list/
+ https://solidarity.international/index.php/abc-groups-around-the-world/  
+ https://brightonantifascists.com/links/
+ https://nothingiseverlost.wordpress.com
+ https://bandilangitim.noblogs.org/friends/
+ https://eyfa.org/how-we-work/network/members-friends/
+ https://www.anarchistfederation.net/illustrated-guide-version-13-6-uploaded/
+ https://lahorde.samizdat.net/groupes-locaux
+ https://www.liquisearch.com/list_of_anarchist_organizations  
